# Screenshot ffmpeg

Script to take screenshots using ffmpeg. Intended for use with bspwm although this is not necessary, it is not compatible with Wayland, only X11.

## Dependencies
* xdotool -- It is necessary to obtain the data of the windows.
* rofi -- It is only necessary for the selection interface.
* wmctrl -- To get the window ID.
