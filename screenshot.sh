#!/bin/bash
typeset -a DEPEN=(xdotool ffmpeg rofi wmctrl) WINS;
typeset -i N_NODES LNS=5;
typeset SELECTION;

# USAGE: ./script.sh [delay: int] [mouse: bool]

SHOT() {
	typeset NAME=$(date +"Screenshot_%a-%b-%d_%H-%M.png") RES=$(xrandr | awk '/*/{print $1}');
	typeset -i BORDER=$(bspc config border_width 2> /dev/null) DES_WIDTH DES_HEIGHT;

	DES_WIDTH=${RES%x*};
	DES_HEIGHT=${RES#*x};
	# shellcheck disable=SC1090
	source <(xdotool getwindowgeometry --shell "$1");

	if (( DES_WIDTH < (X+BORDER+WIDTH) )); then
		echo "The width of the win protrudes beyond the visible area.";
		((WIDTH-=(X+BORDER+WIDTH)-DES_WIDTH));
	fi

	if (( DES_HEIGHT < (Y+BORDER+HEIGHT) )); then
		echo "The height of the win protrudes beyond the visible area.";
		((HEIGHT-=(Y+BORDER+HEIGHT)-DES_HEIGHT));
	fi

	if $3 > /dev/null 2>&1 || [[ $3 -ge 1 ]]; then
		set "$1" "$2" 1;
	else
		set "$1" "$2" 0;
	fi

	sleep "$2" 2> /dev/null;

	bspc node -f "$1" 2> /dev/null;
	# Primero intenta capturar compensando el ancho del borde, cuando esto no
	# funciona puede ser porque es una ventana monocle o sin borde
	ffmpeg -f x11grab -video_size "${WIDTH}x${HEIGHT}" -draw_mouse "$3" \
		-hide_banner -i ":${SCREEN}+$((X+BORDER)),$((Y+BORDER))" -vframes 1 \
		"$NAME" > /dev/null 2>&1 || \
	ffmpeg -f x11grab -video_size "${WIDTH}x${HEIGHT}" -draw_mouse "$3" \
		-hide_banner -i ":${SCREEN}+${X},${Y}" -vframes 1 "$NAME" > /dev/null 2>&1;
}
# Check dependencies
for D in "${DEPEN[@]}"; do
	if ! command -v "$D" > /dev/null 2>&1; then
		echo "«$D» no está instalado";
		exit 1;
	fi
done

if killall -0 bspwm 2> /dev/null; then
	mapfile -t WINS < <(bspc query -N -n '.!hidden.window' 2> /dev/null);

	for N in "${!WINS[@]}"; do
		WINS[$N]+="$(printf '\t%s - %s' \
			"$(ps -p "$(xdotool getwindowpid "${WINS[$N]}" 2> /dev/null)" -o comm= 2> /dev/null)" \
			"$(xdotool getwindowname "${WINS[$N]}" 2> /dev/null)")";
	done
elif command -v wmctrl > /dev/null 2>&1; then
	mapfile -t WINS < <(wmctrl -l 2> /dev/null);
fi

N_NODES="${#WINS[@]}";

if (( N_NODES == 0 )); then
	echo "No se detectaron ventana...";
	exit 0;
elif (( N_NODES == 1 )); then
	SHOT "${WINS[0]%%[[:space:]]*}" "${1:-0}" "${2:-1}";
	exit 0;
elif (( N_NODES < LNS )); then
	LNS=$N_NODES;
fi

if SELECTION="$(printf "%s\n" "${WINS[@]#*[[:space:]]}" | rofi -dmenu -p "Win" \
	-no-custom -i -format i:s -hide-scrollbar -lines "$LNS" -width 35)"; then
	SHOT "${WINS[${SELECTION%%:*}]%%[[:space:]]*}" "${1:-0}" "${2:-1}";
fi
